import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar-menu">
      <div className="navbar-item">
        <div className="buttons">
          <div className="button is-primary">
            <Link to="/">LOGIN </Link>
          </div>
          <div className="button is-light">
            <Link to="/register">REGISTER </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
